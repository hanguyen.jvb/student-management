<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormAddStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'unique:users,email',
            'fileToUpload' => 'mimes:jpeg,jpg,png|max:5120',
        ];
    }

    public function messages()
    {
        return [
            'fileToUpload.mimes' => 'Only upload files png, jpg, jpeg',
            'fileToUpload.max' => 'Maximum file size is 5MB',
            'email.unique' => 'Email already exists',
        ];
    }
}
