<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    public function list()
    {
        return view('login.list');
    }

    public function process(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            return redirect('home');
        } else {
            return redirect()->back()->with('notification', 'The username or password is incorrect')->withInput();
        }
    }
}
