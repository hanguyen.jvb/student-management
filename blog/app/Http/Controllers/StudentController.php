<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\ClassStudent;
use App\Department;
use App\Http\Requests\{FormAddStudent, FormEditStudent};
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StudentController extends Controller
{
    public function list()
    {
        $data['students'] = User::orderBy('id', 'desc')->paginate(3);

        return view('student.list', $data);
    }

    public function create()
    {
        $data['classes'] = ClassStudent::all();
        $data['departments'] = Department::all();

        return view('student.add', $data);
    }

    public function store(FormAddStudent $request)
    {
        $student = new User();
        $student->fullname = $request->fullname;
        $student->username = $request->username;
        $student->slug = Str::slug($request->fullname, '-');
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        $student->phone = $request->phone;
        $student->birthday = $request->birthday;
        $student->gender = $request->gender;
        $student->department_id = $request->department_student;
        $student->class_id = $request->class_student;

        if ($request->hasFile('fileToUpload')) {
            $file = $request->fileToUpload;
            $fileName = Str::slug($request->fullname, '-') . '.' . $file->getClientOriginalExtension();
            $file->move('img/', $fileName);
            $student->img = $fileName;
        }
        $student->save();

        return redirect('home/student')->with('notification', 'Add student successfully')->withInput();
    }

    public function edit($id)
    {
        try {
            $data['idStudent'] = User::findOrFail($id);
            return view('student.edit', $data);
        } catch (ModelNotFoundException  $e) {
            return view('error.404');
        }
    }

    public function update(FormEditStudent $request, $id)
    {
        $student = User::find($id);
        $student->fullname = $request->fullname;
        $student->username = $request->username;
        $student->slug = Str::slug($request->fullname, '-');
        $student->email = $request->email;
        $student->password = bcrypt($request->password);
        $student->phone = $request->phone;
        $student->birthday = $request->birthday;
        $student->gender = $request->gender;
        $student->department_id = $request->department_student;
        $student->class_id = $request->class_student;

        if ($request->hasFile('fileToUpload')) {
            $file = $request->fileToUpload;
            $fileName = Str::slug($request->fullname, '-') . '.' . $file->getClientOriginalExtension();
            $file->move('img/', $fileName);
            $student->img = $fileName;
            $student->save();

            return redirect('home/student')->with('notification', 'Edit student successfully')->withInput();
        }
    }

    public function destroy(Request $request)
    {
        $idCheckboxs = $request->checkbox;
        foreach ($idCheckboxs as $idCheckbox) {
            User::where('id', $idCheckbox)->delete();
        }

        return redirect('home/student')->with('notification', 'Delete student successfully');
    }

    public function search(Request $request)
    {
        if ($request->get('key_search') == '') {
            return redirect('home/student');
        } else {
            $keySearch = $request->get('key_search');
            $data['search'] = User::where('fullname', 'like', '%' . $keySearch . '%')
            ->orWhere('username', 'like', '%' . $keySearch . '%')
            ->join('classes', 'classes.id', '=', 'users.class_id')
            ->orWhere('classes.name', 'like', '%' . $keySearch . '%')
            ->join('departments', 'departments.id', '=', 'users.department_id')
            ->orWhere('departments.name', 'like', '%' . $keySearch . '%')
            ->orWhere('birthday', 'like', '%' . $keySearch . '%')
            ->get();

            return view('student.resultSearch', $data);
        }
    }
}
