<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ClassStudent;
use App\Department;
use App\User;

class HomeController extends Controller
{
    public function list()
    {
        $data['students'] = User::all();
        $data['classes'] = ClassStudent::all();
        $data['departments'] = Department::all();
        return view('home', $data);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
