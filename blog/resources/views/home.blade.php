@push('css')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@endpush
@extends('master.master')
@section('title', 'home')
@section('content')
    <div class="overview">
        <h2>Tổng quan</h2>
        <div class="students">
            <i class="fas fa-users icon"></i>
            <span>number of student</span><br>
            <div class="info">{{ count($students) }} student</div>
        </div>
        <div class="classes">
            <i class="fas fa-users icon"></i>
            <span>number of classes</span><br>
            <div class="info">{{ count($classes) }} class</div>
        </div>
        <div class="departments">
            <i class="fas fa-users icon"></i>
            <span>number of departments</span><br>
            <div class="info">{{ count($departments) }} department</div>
        </div>
    </div>
@endsection
