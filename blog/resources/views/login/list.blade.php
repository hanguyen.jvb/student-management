<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
</head>
<body>
    <div id="login">
        <h2 class="title-login">Login to JVB</h2>
        <div class="login-box">
            <form id="login-form" method="POST">
                <h3>Login</h3>
                @csrf
                @if (session('notification'))
                    <span class="error-login">{{ session('notification') }}</span>
                @endif
                <input type="text" name="username" placeholder="username" value="{{ old('username') }}">
                <input type="password" name="password" placeholder="password">
                <input type="submit" class="btn-login" value="login">
            </form>
        </div>
    </div>
</body>
</html>
