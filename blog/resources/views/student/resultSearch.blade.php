@push('css')
    <link rel="stylesheet" href="{{ asset('css/result-search.css') }}">
@endpush
@extends('master.master')
@section('title', 'result search')
@section('content')
    <h2 style="margin-top: 30px; margin-left: 180px">Kết quả tìm kiếm</h2>
    <table id="table-student">
        <tr>
            <th>Avatar</th>
            <th>Fullname</th>
            <th>Username</th>
            <th>Email</th>
            <th>Class</th>
            <th>Department</th>
            <th>Phone</th>
            <th>Birthday</th>
            <th>Gender</th>
        </tr>
        @foreach ($search as $resultSearch)
            <tr>
                <td><img class="avatar" src="/img/{{ $resultSearch->img }}" alt=""></td>
                <td>{{ $resultSearch->fullname }}</td>
                <td>{{ $resultSearch->username }}</td>
                <td>{{ $resultSearch->email }}</td>
                <td>{{ $resultSearch->ClassStudent->name }}</td>
                <td>{{ $resultSearch->Department->name }}</td>
                <td>{{ $resultSearch->phone }}</td>
                <td>{{ $resultSearch->birthday }}</td>
                <td>
                    @if ($resultSearch->gender == 1)
                        {{ 'Nam' }}
                    @else
                        {{ 'Nữ' }}
                    @endif
                </td>
            <tr>
        @endforeach
    </table>
@endsection
