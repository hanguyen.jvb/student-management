@push('css')
    <link rel="stylesheet" href="{{ asset('css/list-student.css') }}">
@endpush
@extends('master.master')
@section('title', 'list student')
@section('content')
    @if (session('notification'))
        <div class="alert">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            {{ session('notification') }}
        </div>
    @endif
    <div class="search-student">
        <form action="{{ route('user.search') }}" method="get">
            <table>
                <tr>
                    <td><input type="text" class="key-search" name="key_search" placeholder="The name you wanna search"></td>
                    <td><input type="submit" class="btn-search" name="search" value="search"></td>
                </tr>
            </table>
        </form>
    </div>

    <div class="add-student">
        <a href="{{ route('user.create') }}" class="btn-add-student">Add Student +</a>
    </div>
    <form method="POST" action="{{ route('user.destroy') }}" onsubmit="return deleteConfirm();">
        @csrf
        <table id="table-student">
            <tr>
                <th>Id</th>
                <th>Avatar</th>
                <th>Fullname</th>
                <th>Username</th>
                <th>Email</th>
                <th>Class</th>
                <th>Department</th>
                <th>Phone</th>
                <th>Birthday</th>
                <th>Gender</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            @foreach ($students as $student)
                <tr>
                    <td>{{ $student->id }}</td>
                    <td><img class="avatar" src="/img/{{ $student->img }}" alt=""></td>
                    <td>{{ $student->fullname }}</td>
                    <td>{{ $student->username }}</td>
                    <td>{{ $student->email }}</td>
                    <td>{{ $student->ClassStudent->name }}</td>
                    <td>{{ $student->Department->name }}</td>
                    <td>{{ $student->phone }}</td>
                    <td>{{ $student->birthday }}</td>
                    <td>
                        @if ($student->gender == 1)
                            {{ 'Nam' }}
                        @else
                            {{ 'Nữ' }}
                        @endif
                    </td>
                    <td><a href="{{ route('user.edit', $student->id) }}"><i class="fas fa-pencil-alt icon-edit"></i></a></td>
                    <td><input name="checkbox[]" type="checkbox" value="{{ $student->id }}"></td>
                </tr>
            @endforeach
            <input type="submit" name="delete-student" id="delete-student" value="Delete Student">
        </table>
    </form>

    <div class="pagination">
        {{ $students->links() }}
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/student/confirmDelete.js') }}"></script>
@endpush
