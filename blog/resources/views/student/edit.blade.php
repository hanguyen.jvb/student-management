@push('css')
    <link rel="stylesheet" href="{{ asset('css/edit-student.css') }}">
@endpush
@extends('master.master')
@section('title', 'edit student')
@section('content')
    <div id="edit-student">
        <div class="edit-student-box">
            <form class="edit-student-form" method="POST" id="formEditStudent" enctype="multipart/form-data">
                @csrf
                <h3>Edit Student</h3>
                <input type="text" name="fullname" id="fullname" placeholder="update fullname" value="{{ $idStudent->fullname }}">
                <input type="text" name="username" id="username" placeholder="update username" value="{{ $idStudent->username }}">
                <input type="email" name="email" id="email" placeholder="update email" value="{{ $idStudent->email }}">
                <input type="password" name="password" id="password" placeholder="update password">
                <select id="department-student" name="department_student">
                   <option value="1" {{ $idStudent->department_id == 1 ? 'selected' : '' }}>Công nghệ - department</option>
                   <option value="2" {{ $idStudent->department_id == 2 ? 'selected' : '' }}>Điện tử - department</option>
                   <option value="3" {{ $idStudent->department_id == 3 ? 'selected' : '' }}>Viễn thông - department</option>
                   <option value="4" {{ $idStudent->department_id == 4 ? 'selected' : '' }}>Kinh tế - department</option>
                </select>
                <select id="class-student" name="class_student">
                    <option value="1" {{ $idStudent->class_id == 1 ? 'selected' : '' }}>Công nghệ - class</option>
                    <option value="2" {{ $idStudent->class_id == 2 ? 'selected' : '' }}>Điện tử - class</option>
                    <option value="3" {{ $idStudent->class_id == 3 ? 'selected' : '' }}>Viễn thông - class</option>
                    <option value="4" {{ $idStudent->class_id == 4 ? 'selected' : '' }}>Kinh tế - class</option>
                </select>
                <label for="uploadAvatar" style="font-size: 19px;">Update Avatar(just update png, jpg, jpeg)</label>
                <input type="file" name="fileToUpload" id="fileToUpload">
                <div class="error-upload-img">
                    @if ($errors->has('fileToUpload'))
                        {{ $errors->first('fileToUpload') }}
                    @endif
                </div>
                <div class="empty-text prev-img">
                    <img src="/img/{{ $idStudent->img }}" alt="">
                </div>
                <input type="number" name="phone" id="phone" placeholder="update your phone" value="{{ $idStudent->phone }}">
                <input type="date" name="birthday" id="birthday" placeholder="update your birthday" value="{{ $idStudent->birthday }}">
                <div>
                    <input type="radio" id="male" name="gender" value="1" {{ $idStudent->gender == 1 ? 'checked' : '' }}>
                    <label for="male">Male</label>
                    <input type="radio" id="female" name="gender" value="0" {{ $idStudent->gender == 0 ? 'checked' : '' }}>
                    <label for="female">Female</label>
                </div>
                <input type="hidden" name="student_id" value="">
                <input type="submit" name="edit-student" class="btn-edit">
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/student/classByDepartment.js') }}"></script>
    <script src="{{ asset('js/student/validateEditStudent.js') }}"></script>
    <script src="{{ asset('js/student/prevImage.js') }}"></script>
@endpush
