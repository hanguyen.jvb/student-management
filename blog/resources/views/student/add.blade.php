@push('css')
    <link rel="stylesheet" href="{{ asset('css/add-student.css') }}">
@endpush
@extends('master.master')
@section('title', 'add student')
@section('content')
<div id="add-student">
    <div class="add-student-box">
        <form class="add-student-form" method="POST" id="formAddStudent" enctype="multipart/form-data">
            @csrf
            <h3>Add Student</h3>
            <input type="text" name="fullname" id="fullname" value="{{ old('fullname') }}" placeholder="fullname">
            <input type="text" name="username" id="username" value="{{ old('username') }}" placeholder="username">
            <input type="email" name="email" id="email" value="{{ old('email') }}" placeholder="email">
            <div class="error-email-exists">
                @if ($errors->has('email'))
                    {{ $errors->first('email') }}
                @endif
            </div>
            <input type="password" name="password" id="password" placeholder="password">
            <select id="department-student" name="department_student">
                @foreach ($departments as $department)
                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                @endforeach
            </select>
            <select id="class-student" name="class_student" id="class-student">
                @foreach ($classes as $classStudent)
                    <option value="{{ $classStudent->id }}">{{ $classStudent->name }}</option>
                @endforeach
            </select>
            <label for="uploadAvatar" style="font-size: 19px;">Upload Avatar(just upload png, jpg, jpeg)</label>
            <input type="file" name="fileToUpload" id="fileToUpload" data-type='image'>
            <div class="error-upload-img">
                @if ($errors->has('fileToUpload'))
                    {{ $errors->first('fileToUpload') }}
                @endif
            </div>
            <div class="empty-text">Upload images</div>
            <input type="number" name="phone" id="phone" value="{{ old('phone') }}" placeholder="your phone">
            <input type="date" name="birthday" id="birthday" value="{{ old('birthday') }}" placeholder="your birthday">
            <div>
                <input type="radio" id="male" name="gender" value="1"><label for="male">Male</label>
                <input type="radio" id="female" name="gender" value="0"><label for="female">Female</label>
            </div>
            <input type="submit" name="add-student" class="btn-add">
        </form>
    </div>
</div>
@endsection

@push('script')
    <script src="{{ asset('js/student/classByDepartment.js') }}"></script>
    <script src="{{ asset('js/student/validateCreateStudent.js') }}"></script>
    <script src="{{ asset('js/student/prevImage.js') }}"></script>
@endpush
