<div id="header">
    <div id="navbar">
        <ul>
            <li><a href="{{ route('home.list') }}">JVB Viet Nam</a></li>
        </ul>
    </div>
    <div id="info-navbar">
        <span> Hi
        @if (Auth::check())
            {{ Auth::user()->fullname }}
        @endif
        </span> |
        <a href="/home/logout">Logout</a>
    </div>
</div>
