<?php

use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['fullname' => 'nguyen viet ha', 'username' => 'hanv', 'email' => 'ha@gmail.com', 'password' => bcrypt('123456'), 'phone' => '0356653301', 'birthday' => '02/03/1997', 'gender' => '1', 'department_id' => '1', 'class_id' => '1'],
            ['fullname' => 'hoang tien anh', 'username' => 'anhht97', 'email' => 'anhht@gmail.com', 'password' => bcrypt('123456'), 'phone' => '0987456567', 'birthday' => '08/08/1997', 'gender' => '1', 'department_id' => '1', 'class_id' => '1'],
        ]);
    }
}
