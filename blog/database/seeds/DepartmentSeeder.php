<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            ['name' => 'Công nghệ'],
            ['name' => 'Điện tử'],
            ['name' => 'Viễn thông'],
            ['name' => 'Kinh tế'],
        ]);
    }
}
