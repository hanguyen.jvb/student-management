<?php

use Illuminate\Database\Seeder;

class ClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->insert([
            ['name' => 'Công nghệ 1', 'department_id' => '1'],
            ['name' => 'Điện tử 1', 'department_id' => '2'],
            ['name' => 'Viễn thông 1', 'department_id' => '3'],
            ['name' => 'Kinh tế 1', 'department_id' => '4'],
        ]);

    }
}
