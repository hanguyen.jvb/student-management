<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'LoginController@list')->name('login.list')->middleware('CheckLogout');
Route::post('login', 'LoginController@process')->name('login.process');

Route::group(['prefix' => 'home', 'middleware' => 'CheckLogin'], function () {
    Route::get('', 'HomeController@list')->name('home.list');
    Route::get('logout', 'HomeController@logout')->name('home.logout');
    Route::group(['prefix' => 'student'], function () {
        Route::get('', 'StudentController@list')->name('user.list');
        Route::get('add', 'StudentController@create')->name('user.create');
        Route::post('add', 'StudentController@store')->name('user.store');
        Route::get('edit/{id}', 'StudentController@edit')->name('user.edit');
        Route::post('edit/{id}', 'StudentController@update')->name('user.update');
        Route::post('delete', 'StudentController@destroy')->name('user.destroy');
        Route::get('search', 'StudentController@search')->name('user.search');
    });
});
