$(document).ready(function () {
    $("#formAddStudent").validate({
        rules: {
            fullname: "required",
            username: "required",
            email: "required",
            password: {
                required: true,
                minlength: 6
            },
            fileToUpload: "required",
            phone: {
                required: true,
                minlength: 10,
                maxlength: 11
            },
            birthday: {
                required: true
            },
        },
        messages: {
            fullname: "Please enter your fullname",
            username: "Please enter your username",
            email: "Please enter your email",
            password: {
                required: "Please enter your password",
                minlength: "Your password is from 6 characters"
            },
            fileToUpload: "Choose your file to upload",
            phone: {
                required: "Please enter your phone",
                minlength: "Your phone number from 10 to 11 numbers",
                maxlength: "Your phone number from 10 to 11 numbers"
            },
            birthday: "Please enter your birthday",
        }
    });
    $("#formAddStudent").validate({
        onsubmit: false
    });
})
