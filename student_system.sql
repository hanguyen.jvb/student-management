-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2020 at 09:01 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `name`, `department_id`) VALUES
(1, 'Công nghệ 1', 1),
(2, 'Điện tử 1', 2),
(3, 'Viễn thông 1', 3),
(4, 'Kinh tế 1', 4);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`) VALUES
(1, 'Công nghệ'),
(2, 'Điện tử'),
(3, 'Viễn thông'),
(4, 'Kinh tế');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2020_04_17_032847_create_departments_table', 1),
(5, '2020_04_17_041435_create_classes_table', 1),
(6, '2020_04_17_041505_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `class_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `slug`, `img`, `email`, `password`, `phone`, `birthday`, `gender`, `department_id`, `class_id`) VALUES
(36, 'hoang tien anh', 'anhht97', 'hoang-tien-anh', 'hoang-tien-anh.jpg', 'anhht@gmail.com', '$2y$10$yW9d8q0i/E4GBvQr4MCN4O0jq1QwZtAW6FVsRIV2hbikfngTmjXO.', '0978987567', '1997-08-08', 1, 1, 1),
(37, 'nguyen thu thao', 'thaont97', 'nguyen-thu-thao', 'nguyen-thu-thao.jpg', 'thaont@gmail.com', '$2y$10$t.74p56WdqDpDNo5BEyCLeKOBGMDRHo/gv.Hil2mdMscHr.KGiCNq', '0962678789', '1997-05-19', 0, 4, 4),
(38, 'vu trung kien', 'kienvt97', 'vu-trung-kien', 'vu-trung-kien.jpg', 'kien@gmail.com', '$2y$10$XS1FYY3yTN.VcYVCqVnjaupmIbxZmtVjcmVmKAyEf/VYurxy4fq.W', '0169667877', '1997-11-05', 1, 1, 1),
(39, 'vinh xuan nguyen', 'vinhnx98', 'vinh-xuan-nguyen', 'vinh-xuan-nguyen.jpg', 'vinh@gmail.com', '$2y$10$u3jldMj3GiGcM.abfAzAbeRao8ebZ4w.oeVUjdsl.PBkz8DsWBkUi', '0169778976', '1998-09-23', 1, 1, 1),
(40, 'nguyen phuong thuy', 'thuynp97', 'nguyen-phuong-thuy', 'nguyen-phuong-thuy.jpg', 'thuy@gmail.com', '$2y$10$/v83wL6crKAWTNLjuAye4enD7tXxneWRzxCixfhtGrJmCKQOJKhGC', '0345678778', '1997-07-27', 0, 2, 2),
(41, 'pham anh my', 'mypa97', 'pham-anh-my', 'pham-anh-my.jpg', 'my@gmail.com', '$2y$10$qksOGxPe3Cwk3CaUtszqyOWK5KwOjbXkvqCbDj5NA5NvLX4rx7iCK', '0345678345', '1997-02-09', 1, 2, 2),
(42, 'trang huyen cao', 'tranghuyen97', 'trang-huyen-cao', 'trang-huyen-cao.jpg', 'trang@gmail.com', '$2y$10$BPiRvmmudywppR7mwn.fEejmaxwjt15SjWN434rjsuEUQnTCWwrsm', '0356877678', '1997-07-25', 0, 4, 4),
(43, 'linh ngo phuong', 'linhnp97', 'linh-ngo-phuong', 'linh-ngo-phuong.jpg', 'linh@gmail.com', '$2y$10$nxqJiwe9.BTCSfHaz.O0xeKerZB48E4TmuIxeyqeAnz3Kp8fN87tK', '01674271768', '1997-10-05', 0, 4, 4),
(44, 'nguyen manh hiep', 'hiepnp2k', 'nguyen-manh-hiep', 'nguyen-manh-hiep.jpg', 'hieu@gmail.com', '$2y$10$xhzu6CRKEVsK1eGg.kd5CuXs6UBZ.OVitS0mI2J/24uqZntJ66qoa', '0345677567', '2000-08-25', 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classes_department_id_foreign` (`department_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_department_id_foreign` (`department_id`),
  ADD KEY `users_class_id_foreign` (`class_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  ADD CONSTRAINT `users_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
